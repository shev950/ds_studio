$(document).ready(function () {

    $('.grid').masonry({
        itemSelector: '.grid-item',
        columnWidth: '.grid-sizer',
        percentPosition: true
    });

    $(".nav__button-hamburger").click(function () {
        let buttonElement = $(this),
            menuElement = $(".nav__menu-hamburger_list"),
            closeIconElement = $(".cross"),
            openIconElement = $(".hamburger");
        if (buttonElement.hasClass('active')) {
            menuElement.fadeToggle(300);
            closeIconElement.fadeOut(150, function () {
                openIconElement.fadeIn(150);
            });
            buttonElement.removeClass('active');
        }
        else {
            menuElement.fadeToggle(300);
            openIconElement.fadeOut(150, function() {
                closeIconElement.fadeIn(150);
            });
            buttonElement.addClass('active');
        }
    });

    // $('.main__items li a').on('click', function(){
    //     $('.main__items li a.current').removeClass('fas');
    //     $(this).addClass('fas');
    // });

});
